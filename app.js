const express = require('express')
const app = express()
const port = 3000

// GET Endpoint - http://localhost:3000/
app.get('/', (req, res) => {
  res.send('Hello World!')
})

// GET multiply endpoint - http://127.0.0.1:3000/multiply?a=3&b=5
app.get('/multiply', (req, res) => {
    try {
        const multiplicant = parseFloat(req.query.a);
        const multiplier = parseFloat(req.query.b);
        if (isNaN(multiplicant)) throw new Error('Invalid multiplicant value');
        if (isNaN(multiplier)) throw new Error('Invalid multiplier value');
        console.log({ multiplicant, multiplier });
        const product = multiply(multiplicant, multiplier);
        res.send(product.toString(10));
    } catch (err) {
        console.error(err.message);
        res.send("Couldn't calculate the product. Try again.");
    }
});

/**
* This function calculates product between two numbers.
* @param {number} multiplier number used for multiplication (factor)
* @param {number} multiplicant number being multiplied
* @returns {number} product
*/
const multiply = (multiplier, multiplicant) => {
    const product = multiplier * multiplicant;
    return product;
};

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})